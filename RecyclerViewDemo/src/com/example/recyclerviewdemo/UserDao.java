package com.example.recyclerviewdemo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserDao {

    public static List<User> getUserList() {
        List<User> userList = new ArrayList<User>();
        for (int n = 0; n < 20; n++) {
            User user = new User();
            user.setId("10000" + n);
            user.setName("Tom" + n);
            user.setPwd(UUID.randomUUID().toString());
            user.setEmail("takwolf@foxmail.com");
            userList.add(user);
        }
        return userList;
    }

}
